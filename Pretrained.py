"""
File demoing the QLearning AI in the 4x4 mode which has played 400 000 games. It averages 14.2/16
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import Snake
import numpy as np


def main():
    print("Demonstration of a Q_learning table trained for 400k generations. It averages 14.2/16")
    ai = np.load("14_2_average.npy")
    Snake.start_game(size=4, good_move_reward=-2, bad_move_reward=-5, eat_reward=50, death_reward=-800,
                     lr=0.3, gamma=0.99, epsilon=0.005, mode="4x4", generation_cnt=10000, q_table=ai)


if __name__ == '__main__':
    main()
