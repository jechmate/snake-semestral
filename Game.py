"""
File with the snake game engine
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import random
import numpy as np


class Body:
    """
    Represents one block of the snake
    self.parent: the next block in the body following this one (from tail to head)
    self.x, self.y: coordinates
    """

    def __init__(self, parent, x, y):
        self.parent = parent
        self.x = x
        self.y = y

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def set_pos(self, pos):
        self.x = pos[0]
        self.y = pos[1]

    def set_parent(self, parent):
        self.parent = parent

    def get_pos(self):
        return self.x, self.y


class Snake:
    """
    Represents the snakes body blocks
    self.body: list of body blocks where the last element is the tail
    """

    def __init__(self, x, y):
        self.body = []
        self.body.append(Body(None, x, y))

    def move(self, direction):
        """
        Moves the snake in the given direction
        :param direction: The direction of movement - 0 is up, 1 is right, 2 is down and 3 is left
        :return: Position of the old tail and new head position as a tuple of 4 integers
        """
        old_tail_pos = self.get_tail().get_pos()
        old_head_pos = self.get_head().get_pos()
        for piece in self.body[:-1]:
            piece.set_pos(piece.parent.get_pos())
        if direction == 0:
            self.get_head().set_pos((old_head_pos[0], old_head_pos[1] + 1))
        elif direction == 1:
            self.get_head().set_pos((old_head_pos[0] + 1, old_head_pos[1]))
        elif direction == 2:
            self.get_head().set_pos((old_head_pos[0], old_head_pos[1] - 1))
        elif direction == 3:
            self.get_head().set_pos((old_head_pos[0] - 1, old_head_pos[1]))

    def extend(self, x, y):
        """
        Extends the snake onto the x,y coordinates
        :param x: x coordinate of extension
        :param y: y coordinate of extension
        """
        self.body.append(Body(None, x, y))
        self.body[-2].set_parent(self.body[-1])

    def get_head(self):
        return self.body[-1]

    def get_tail(self):
        return self.body[0]

    def get_length(self):
        return len(self.body)


class Game:
    """
    Defines the snake game engine
    self.body_id, head_id, food_id - values used for defining different blocks on the playing board
    size - size of the square playing field
    good_move_reward - reward for moving towards the food
    bad_move_reward - reward for moving away from the food
    death_reward - reward for losing the game
    eat_reward - reward for eating food and increasing score
    mode - determines what type of AI do we want. Free can play on any board size
           but has less information about the game. 4x4 can only play on size == 4,
           but has all of the information about the board
    state_count - the number of states the game can be in - in the eyes of the QTable
    board - numpy array representation of the game board
    snake - the snake in this grass
    state - the state of the game
    state_func - the function used to calculate this games state - changes with mode
    food_pos - position of the food
    reward - reward for the current move
    """
    def __init__(self, size=4, good_move_reward=3, bad_move_reward=-1, death_reward=-5, eat_reward=10, mode="free"):
        self.body_id = 1
        self.head_id = 2
        self.food_id = 3
        self.good_move_reward = good_move_reward
        self.bad_move_reward = bad_move_reward
        self.death_reward = death_reward
        self.eat_reward = eat_reward
        self.size = size
        self.mode = mode
        if self.mode == "4x4":
            self.size = 4
            self.state_count = 2 ** 24
            self.state_func = self.calc_state_full
        else:
            self.mode = "free"
            self.state_count = 2 ** 12
            self.state_func = self.calc_state_flexible
        self.board = np.zeros([size, size], dtype=np.int8)
        start_x = random.randint(0, size - 1)
        start_y = random.randint(0, size - 1)
        self.board[start_y, start_x] = self.head_id
        self.snake = Snake(start_x, start_y)
        self.state = np.uint16(0)
        self.food_pos = (0, 0)
        self.reward = 0
        self.spawn_food()
        self.state_func()

    def get_settings(self):
        return self.size, self.good_move_reward, self.bad_move_reward, self.death_reward, self.eat_reward, self.mode

    def get_state_cnt(self):
        return self.state_count

    def get_board(self):
        return self.board

    def spawn_food(self):
        """
        Spawns the food in a random unoccupied location
        """
        empty = []
        for index, value in np.ndenumerate(self.board):
            if value == 0:
                empty.append(index)
        if not empty:
            return False
        choice = random.choice(empty)
        self.food_pos = (choice[1], choice[0])
        self.board[choice] = self.food_id
        return True

    def food_dir_check_full(self, direction):
        """
        Checks if the food is in the given direction - all 8 directions
        :param direction: the direction to be checked
        :return: Bool - is the food in the given direction
        """
        head_pos = self.snake.get_head().get_pos()
        food_head_dif = (self.food_pos[0] - head_pos[0], self.food_pos[1] - head_pos[1])
        if direction == 0:
            if food_head_dif[1] > 0 and food_head_dif[0] == 0:
                return True
        elif direction == 1:
            if food_head_dif[1] > 0 and food_head_dif[0] > 0:
                return True
        elif direction == 2:
            if food_head_dif[1] == 0 and food_head_dif[0] > 0:
                return True
        elif direction == 3:
            if food_head_dif[0] > 0 and food_head_dif[1] < 0:
                return True
        elif direction == 4:
            if food_head_dif[0] == 0 and food_head_dif[1] < 0:
                return True
        elif direction == 5:
            if food_head_dif[0] < 0 and food_head_dif[1] < 0:
                return True
        elif direction == 6:
            if food_head_dif[0] < 0 and food_head_dif[1] == 0:
                return True
        elif direction == 7:
            if food_head_dif[0] < 0 and food_head_dif[1] > 0:
                return True
        return False

    def food_dir_check(self, direction):
        """
        Checks if the food is in the given direction - 4 directions
        :param direction: the direction to be checked
        :return: Bool - is the food in the given direction
        """
        head_pos = self.snake.get_head().get_pos()
        food_head_dif = (self.food_pos[0] - head_pos[0], self.food_pos[1] - head_pos[1])
        if direction == 0:
            if food_head_dif[1] > 0:
                return True
        elif direction == 1:
            if food_head_dif[0] > 0:
                return True
        elif direction == 2:
            if food_head_dif[1] < 0:
                return True
        elif direction == 3:
            if food_head_dif[0] < 0:
                return True
        return False

    def calc_state_full(self):
        """
        Calculates the state number. THis is the full version used in 4x4 mode. 16 bits are used to tell if each spot
        on the board is or is not occupied. Next 4 bits are used for the position of the head,
        last 4 are for the food position
        :return: integer - number of the state
        """
        state_string = ""
        head_pos = self.snake.get_head().get_pos()
        head_index = head_pos[0] + self.size * head_pos[1]
        food_index = self.food_pos[0] + self.size * self.food_pos[1]
        for x in range(0, self.size):
            for y in range(0, self.size):
                if self.board[y, x] == 0 or self.board[y, x] == self.food_id:
                    state_string += "0"
                else:
                    state_string += "1"
        while head_index != 0:
            state_string += str(head_index % 2)
            head_index = int(head_index / 2)
        while food_index != 0:
            state_string += str(food_index % 2)
            food_index = int(food_index / 2)
        self.state = np.uint32(int(state_string, 2))
        return self.state

    def calc_state_flexible(self):
        """
        Calculates the state number. THis is the full version used in free mode. 8 bits are for the 8 positions around
        the head and determine if the block is occupied or not. Next 3 bits are used to store one of the 8 directions
        the food can be in relative to the head.
        :return: integer - number of the state
        """
        state_string = ""
        food_dir = 0
        head_pos = self.snake.get_head().get_pos()
        for x in range(head_pos[0] - 1, head_pos[0] + 2):
            for y in range(head_pos[1] - 1, head_pos[1] + 2):
                if (x, y) == head_pos:
                    continue
                elif x < 0 or x >= self.size or y < 0 or y >= self.size:
                    state_string += "1"
                elif self.board[y, x] == self.body_id:
                    state_string += "1"
                elif self.board[y, x] == 0:
                    state_string += "0"
        for i in range(8):
            if self.food_dir_check_full(i):
                food_dir = i
                break
        while food_dir != 0:
            state_string += str(food_dir % 2)
            food_dir = int(food_dir / 2)
        self.state = np.uint16(int(state_string, 2))
        return self.state

    def potential_pos(self, direction):
        """
        Calculates the potential position of the head after moving in a given direction
        :param direction: The direction of the move
        :return: The potential new head positions
        """
        new_x, new_y = self.snake.get_head().get_pos()
        if direction == 0:
            new_y += 1
        elif direction == 1:
            new_x += 1
        elif direction == 2:
            new_y -= 1
        elif direction == 3:
            new_x -= 1
        return new_x, new_y

    def try_move(self, direction):
        """
        Tries to move in a given direction
        :param direction: The direction of the move
        :return: bool - if the move hits a solid block or not
        """
        new_x, new_y = self.potential_pos(direction)
        if new_x == -1 or new_x == self.size:
            return False
        elif new_y == -1 or new_y == self.size:
            return False
        elif self.board[new_y, new_x] == self.body_id:
            return False
        else:
            return True

    def move_snake(self, direction):
        """
        Moves the snake in a given direction
        :param direction: The direction of the move
        :return: state of the game, reward gained, if the game is over or not, current score
        """
        game_over = False
        self.reward = 0
        if self.try_move(direction):
            if self.food_dir_check(direction):
                self.reward = self.good_move_reward
            else:
                self.reward = self.bad_move_reward
            head_x, head_y = self.snake.get_head().get_pos()
            self.board[head_y, head_x] = self.body_id
            pot_x, pot_y = self.potential_pos(direction)
            if self.board[pot_y, pot_x] == self.food_id:
                self.snake.extend(pot_x, pot_y)
                self.board[pot_y, pot_x] = self.head_id
                if not self.spawn_food():
                    game_over = True
                self.reward = self.eat_reward
            else:
                old_tail_pos = self.snake.get_tail().get_pos()
                self.board[pot_y, pot_x] = self.head_id
                self.snake.move(direction)
                self.board[old_tail_pos[1], old_tail_pos[0]] = 0
        else:
            self.reward = self.death_reward
            game_over = True
        return self.state_func(), self.reward, game_over, self.snake.get_length()
