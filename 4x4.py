"""
File demoing the 4x4 Q_Learning
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import Snake


def main():
    print("Starting a demonstration of q_learning playing a game of 4x4 snake.\n"
          "This version has all of the information a human would have.\n"
          "Information if a block is occupied = 16 bits. Exact location of head and food - 2*4 bits.\n"
          "Total number of states is 2^24.\n"
          "This version takes a long time to learn but has the potential of playing near perfect games.\n"
          "Check Pretrained.py for the showcase of its potential.")
    Snake.start_game(size=4, good_move_reward=-2, bad_move_reward=-5, eat_reward=50, death_reward=-800,
                     lr=0.3, gamma=0.99, epsilon=0.005, mode="4x4", generation_cnt=10000)


if __name__ == '__main__':
    main()
