"""
File creating and running the visualisation of example games using Pyglet
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import pyglet
import time
import numpy as np
from Q_learning import QLearning
from Game import Game

main_batch = pyglet.graphics.Batch()
pyglet.resource.path = ['./Resources']
pyglet.resource.reindex()
body_image = pyglet.resource.image("Body.png")
food_image = pyglet.resource.image("Food.png")
head_image = pyglet.resource.image("Head.png")
empty_image = pyglet.resource.image("Empty.png")
border_image = pyglet.resource.image("Border.png")


def erase_board(board):
    """
    Erases the playing board
    :param board: The board to be erased
    """
    for x in range(0, int(len(board)/len(board))):
        for y in range(0, int(len(board)/len(board))):
            board[(x, y)].delete()
            board[(x, y)] = \
                pyglet.sprite.Sprite(img=empty_image, x=x * 50 + 50, y=y * 50 + 50, batch=main_batch)


class Visualise:
    """
    A class that runs snake games and updates the playing board.
    game_over - if the game has ended this turn or not
    event_loop - the Pyglet event loop which controls the window
    draw_board - the playing board
    window - the window in which Pyglet draws the game
    q_learning - the AI that plays the game
    settings - the settings used for this game (explained in Game.py)
    game - the Game class instance of this game
    state - the state in which the game is (integer)
    last_update - holds the time at which the game window was last updated
    examples - the array of example QLearning QTables
    next_id - the index of the next example to be shown
    old_score - holds the score of the game before this turn - used for breaking infinite loops
    score - current score of the game
    stuck - number of moves the AI has played without increasing its score
    """
    def __init__(self, examples, ai, draw_board, win, event_loop, q_table=None):
        if q_table is not None:
            self.info_label = pyglet.text.Label(text="Generation 400k",
                                                x=win.width // 2, y=win.height - 25, anchor_x='center',
                                                batch=main_batch)
        else:
            self.info_label = pyglet.text.Label(text="Generation 0",
                                                x=win.width // 2, y=win.height - 25, anchor_x='center',
                                                batch=main_batch)
        self.score_label = pyglet.text.Label(text="Score: 0",
                                             x=0, y=win.height - 25, anchor_x='left',
                                             batch=main_batch)
        self.game_over = False
        self.event_loop = event_loop
        self.draw_board = draw_board
        self.window = win
        self.q_learning = ai
        self.settings = self.q_learning.get_game_settings()
        self.game = Game(self.settings[0], self.settings[1], self.settings[2],
                         self.settings[3], self.settings[4], self.settings[5])
        self.state = self.game.state_func()
        self.last_update = time.time()
        if q_table is None:
            self.examples = examples
        else:
            self.examples = [q_table, q_table]
        self.q_table = examples[0]
        self.next_id = 0
        self.old_score = 0
        self.score = 0
        self.stuck = 0

    def update_board(self):
        """
        Updates the playing board using the data from draw_board
        """
        cur_board = self.game.get_board()
        self.score_label.delete()
        self.score_label = pyglet.text.Label(text="Score: " + str(self.score),
                                             x=0, y=self.window.height - 25, anchor_x='left',
                                             batch=main_batch)
        for x in range(0, cur_board.shape[0]):
            for y in range(0, cur_board.shape[1]):
                if cur_board[y, x] == 0:
                    self.draw_board[(x, y)].delete()
                    self.draw_board[(x, y)] = \
                        pyglet.sprite.Sprite(img=empty_image, x=x * 50 + 50, y=y * 50 + 50, batch=main_batch)
                elif cur_board[y, x] == self.game.body_id:
                    self.draw_board[(x, y)].delete()
                    self.draw_board[(x, y)] = \
                        pyglet.sprite.Sprite(img=body_image, x=x * 50 + 50, y=y * 50 + 50, batch=main_batch)
                elif cur_board[y, x] == self.game.food_id:
                    self.draw_board[(x, y)].delete()
                    self.draw_board[(x, y)] = \
                        pyglet.sprite.Sprite(img=food_image, x=x * 50 + 50, y=y * 50 + 50, batch=main_batch)
                elif cur_board[y, x] == self.game.head_id:
                    self.draw_board[(x, y)].delete()
                    self.draw_board[(x, y)] = \
                        pyglet.sprite.Sprite(img=head_image, x=x * 50 + 50, y=y * 50 + 50, batch=main_batch)

    def play(self, dt):
        """
        Function that gets called each frame. Controls the game being played
        :param dt: Has to be given for the pyglet.clock.schedule_interval function
        """
        cur_time = time.time()
        if cur_time - self.last_update >= 0.1 and not self.game_over:
            possible_qs = self.q_table[self.state, :]
            action = np.argmax(possible_qs)
            self.state, reward, self.game_over, self.score = self.game.move_snake(action)
            self.update_board()
            self.last_update = time.time()
            if self.score == self.old_score:
                self.stuck += 1
                if self.stuck >= self.q_learning.get_size() * 3 + self.q_learning.get_size():
                    self.stuck = 0
                    self.game_over = True
            else:
                self.stuck = 0
                self.old_score = self.score
        elif self.game_over:
            time.sleep(1)
            self.info_label.delete()
            text = ""
            if len(self.examples) > 2:
                text = "Generation " + str(int(self.q_learning.generation_cnt /
                                               (len(self.examples) - 1)) * self.next_id)
            else:
                text = "Generation 400k"
            self.info_label = pyglet.text.Label(text=text,
                                                x=self.window.width // 2, y=self.window.height - 25, anchor_x='center',
                                                batch=main_batch)
            erase_board(self.draw_board)
            self.next_id += 1
            if self.next_id >= len(self.examples):
                self.next_id = 0
            self.q_table = self.examples[self.next_id]
            self.game = Game(self.settings[0], self.settings[1], self.settings[2],
                             self.settings[3], self.settings[4], self.settings[5])
            self.game_over = False


def start_game(size=10, good_move_reward=-2, bad_move_reward=-8, eat_reward=50, death_reward=-50,
               lr=0.25, gamma=0.995, epsilon=0.01, mode="free", generation_cnt=2000, q_table=None):
    """
    Starts a game and visualises it.
    :param size: size of the square playing field
    :param good_move_reward: reward for moving towards the food
    :param bad_move_reward: reward for moving away from the food
    :param eat_reward: reward for eating food and increasing score
    :param death_reward: reward for losing the game
    :param lr: the learning rate of the ai. Determines how fast the AI picks up new knowledge
    :param gamma: the degree by which the AI trusts what it has learnt earlier
    :param epsilon: determines the chance the AI makes a random move rather than a move determined by the QTable
    :param mode: determines what type of AI do we want. Free can play on any board size
                 but has less information about the game. 4x4 can only play on size == 4,
                 but has all of the information about the board
    :param generation_cnt: Number of games the AI is set to play
    :param q_table: if this parameter is set then the visualisation uses this ATable as the AIs brain
    """
    q_learning = QLearning(size, good_move_reward, bad_move_reward, eat_reward, death_reward,
                           lr, gamma, epsilon,
                           mode, generation_cnt)
    win = pyglet.window.Window((q_learning.get_size() + 2) * 50, (q_learning.get_size() + 2) * 50 + 50)
    borders = []
    for i in range(0, (q_learning.get_size() + 2) * 50, 50):
        new_border = pyglet.sprite.Sprite(img=border_image, x=i, y=0, batch=main_batch)
        new_border_top = pyglet.sprite.Sprite(img=border_image, x=i, y=(q_learning.get_size() + 1) * 50,
                                              batch=main_batch)
        borders.append(new_border)
        borders.append(new_border_top)
    for i in range(50, (q_learning.get_size() + 2) * 50, 50):
        new_border = pyglet.sprite.Sprite(img=border_image, x=0, y=i, batch=main_batch)
        new_border_right = pyglet.sprite.Sprite(img=border_image, x=(q_learning.get_size() + 1) * 50, y=i,
                                                batch=main_batch)
        borders.append(new_border)
        borders.append(new_border_right)
    draw_board = {}
    for i, k in enumerate(range(50, q_learning.get_size() * 50 + 1, 50)):
        for j, l in enumerate(range(50, q_learning.get_size() * 50 + 1, 50)):
            draw_board[(i, j)] = pyglet.sprite.Sprite(img=empty_image, x=k, y=l, batch=main_batch)

    @win.event
    def on_draw():
        win.clear()
        main_batch.draw()

    @win.event
    def on_close():
        win.clear()
        event_loop.exit()
        win.close()
    event_loop = pyglet.app.EventLoop()
    if q_table is None:
        q_learning.train()
        vis = Visualise(q_learning.get_examples(), q_learning, draw_board, win, event_loop)
    else:
        vis = Visualise(q_learning.get_examples(), q_learning, draw_board, win, event_loop, q_table)
    pyglet.clock.schedule_interval(vis.play, 1 / 144.0)

    @event_loop.event
    def on_window_close(window):
        event_loop.exit()
        return pyglet.event.EVENT_HANDLED

    event_loop.run()


if __name__ == '__main__':
    start_game()
