"""
File used for testing of the QLearning class
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import unittest
from Q_learning import QLearning
from matplotlib import pyplot as plt


class LearningTest(unittest.TestCase):
    """
    Testing class for Q_Learning. Tests determine if AI actually learns to play the game better and plots its progress.
    """

    def test_4x4(self):
        """
        Tests and plots the progress of 4x4 AI. The same parameters are used in the 4x4.py file
        """
        test_ai = QLearning(size=4, good_move_reward=-2, bad_move_reward=-5, eat_reward=50, death_reward=-800,
                            lr=0.3, gamma=0.99, epsilon=0.005, mode="4x4", generation_cnt=10000)
        test_ai.train()
        res = test_ai.get_testing()
        self.assertTrue(res[1][0] < res[1][-1])
        plt.plot(res[0], res[1])
        plt.title("Learning progress")
        plt.xlabel("Generation number")
        plt.ylabel("Average score")
        plt.show()

    def test_10x10(self):
        """
        Tests and plots the progress of 10x10 AI. The same parameters are used in the 10x10.py file
        """
        test_ai = QLearning(size=10, good_move_reward=-2, bad_move_reward=-8, eat_reward=50, death_reward=-50,
                            lr=0.25, gamma=0.995, epsilon=0.01, mode="free", generation_cnt=2000)
        test_ai.train()
        res = test_ai.get_testing()
        self.assertTrue(res[1][0] < res[1][-1])
        plt.plot(res[0], res[1])
        plt.title("Learning progress")
        plt.xlabel("Generation number")
        plt.ylabel("Average score")
        plt.show()


if __name__ == '__main__':
    unittest.main()
