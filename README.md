# Snake semestral

Semestrální práce pro BI-PYT a BI-ZUM

Soubor jechmate.pdf obsahuje protokol a detailní popis projektu

Soubor 4x4.py spustí učení a vizualizaci hry ve verzi pro 4x4 pole

Soubor 10x10.py spustí učení a vizualizaci hry ve verzi pro libovolnou velikost pole na herním poli 10x10

Soubor Pretrained.py spustí ukázku hraní hry Had předem naučenou umělou inteligencí. Její průměrné skóre je 14,2 bodu.
