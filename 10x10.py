"""
File demoing the 10x10 Q_Learning
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import Snake


def main():
    print("Starting a demonstration of q_learning playing a game of 10x10 snake.\n"
          "It \"sees\" 8 blocks around the head and the direction towards the food (8 directions).\n"
          "Total number of states is 2^11.")
    Snake.start_game(size=10, good_move_reward=-2, bad_move_reward=-8, eat_reward=50, death_reward=-50,
                     lr=0.25, gamma=0.995, epsilon=0.01, mode="free", generation_cnt=2000)


if __name__ == '__main__':
    main()
