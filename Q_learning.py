"""
File declaring the QLearning class and its methods
author - Matěj Jech, jechmate@fit.cvut.cz
"""
import random
from Game import Game
import numpy as np


class QLearning:
    """
    This class represents the intelligence that learns to play the Snake game.
        size - size of the square playing field
        good_move_reward - reward for moving towards the food
        bad_move_reward - reward for moving away from the food
        death_reward - reward for losing the game
        eat_reward - reward for eating food and increasing score
        mode - determines what type of AI do we want. Free can play on any board size
               but has less information about the game. 4x4 can only play on size == 4,
               but has all of the information about the board
        actions_cnt - number of possible actions. Each turn the snake can move in 4 different directions
        averages - stores the average score of different generations throughout learning
        example_generations - numbers of generations which are used in examples
        q_table - the QTable that stores the knowledge the AI has.
                  Its dimensions are the number of possible states * number of possible actions
        lr - the learning rate of the ai. Determines how fast the AI picks up new knowledge
        gamma - the degree by which the AI trusts what it has learnt earlier
        epsilon - determines the chance the AI makes a random move rather than a move determined by the QTable
        generation_cnt - Number of games the AI is set to play
        index - used for showcasing examples - index of the examples array
        examples - array of QTables used for showcasing the progress of learning
    """
    def __init__(self, size=4, good_move_reward=3, bad_move_reward=-1, eat_reward=10, death_reward=-5,
                 lr=0.85, gamma=0.2, epsilon=0.1, mode="free", generation_cnt=100000):
        self.size = size
        self.good_move_reward = good_move_reward
        self.bad_move_reward = bad_move_reward
        self.death_reward = death_reward
        self.eat_reward = eat_reward
        self.mode = mode
        if mode == "4x4":
            self.state_count = 2 ** 24
            self.size = 4
        else:
            self.mode = "free"
            self.state_count = 2 ** 11
        self.actions_cnt = 4
        self.averages = []
        self.example_generations = []
        self.q_table = np.zeros((self.state_count, self.actions_cnt), dtype=np.float)
        self.lr = lr
        self.gamma = gamma
        self.epsilon = epsilon
        self.generation_cnt = generation_cnt
        self.index = 0
        self.examples = np.zeros([11, self.state_count, self.actions_cnt],
                                 dtype=np.float)

    def get_game_settings(self):
        return self.size, self.good_move_reward, self.bad_move_reward, self.death_reward, self.eat_reward, self.mode

    def get_testing(self):
        return self.example_generations, self.averages

    def set_q_table(self, new_q):
        self.q_table = new_q

    def get_size(self):
        return self.size

    def get_examples(self):
        return self.examples

    def test_ai(self, gen_num, q_table, run_cnt):
        """
        Lets the AI represented bz the Q%table parameter play run_cnt games and calculates its average score
        :param gen_num: How many generations has the AI trained for so far (used only for print)
        :param q_table: Represents the AI
        :param run_cnt: Number of runs the AI is supposed to run
        :return:
        """
        scores = []
        stuck = 0
        for i in range(run_cnt):
            game = Game(self.size, self.good_move_reward, self.bad_move_reward,
                        self.death_reward, self.eat_reward, self.mode)
            state = game.state_func()
            score = 0
            old_score = score
            game_over = False
            while not game_over:
                possible_qs = q_table[state, :]
                action = np.argmax(possible_qs)
                state, reward, game_over, score = game.move_snake(action)
                if score == old_score:
                    stuck += 1
                    if stuck >= self.size ** 2 + self.size * 2:
                        stuck = 0
                        game_over = True
                else:
                    stuck = 0
                    old_score = score
            scores.append(score)
        average_score = np.average(scores)
        best_score = np.argmax(scores)
        self.example_generations.append(gen_num)
        self.averages.append(average_score)
        print("Generation", gen_num, "\n\tAverages:", average_score, "\n\tBest:", scores[best_score])

    def train(self):
        """
        Trains the AI for self.generation_cnt games.
        While the game is not over, it decides by chance if the move should be random
        or deterministic (taken from the q_table). After every move it updates the q_table with the reward it got for
        making a move. https://en.wikipedia.org/wiki/Q-learning
        After every tenth of the total generations it runs the test_ai function and stores its QTable.
        This is used for showcasing the examples
        :return:
        """
        for gen in range(self.generation_cnt + 1):
            turns = 0
            game = Game(self.size, self.good_move_reward, self.bad_move_reward,
                        self.death_reward, self.eat_reward, self.mode)
            state = game.state_func()
            game_over = False
            while not game_over:
                turns += 1
                # if gen <= int(self.generation_cnt * 9/10) and random.uniform(0, 1) < self.epsilon:
                if random.uniform(0, 1) < self.epsilon:
                    action = random.randint(0, 3)
                else:
                    possible_qs = self.q_table[state, :]
                    action = np.argmax(possible_qs)
                new_state, reward, game_over, score = game.move_snake(action)
                # self.q_table[state, action] = reward + self.gamma * np.max(self.q_table[new_state, :])
                self.q_table[state, action] =\
                    self.q_table[state, action] + self.lr *\
                    (reward + self.gamma * np.max(self.q_table[new_state, :]) - self.q_table[state, action])
                state = new_state
            if gen % int(self.generation_cnt/10) == 0:
                # file_name = "q_table" + str(gen) + ".npy"
                self.test_ai(gen, self.q_table, 100)
                self.examples[self.index] = np.copy(self.q_table)
                # np.save(file_name, self.q_table)
                self.index += 1
        self.index = 0


if __name__ == "__main__":
    ai = QLearning(size=10, good_move_reward=-1, bad_move_reward=-3, eat_reward=30, death_reward=-20, mode="free")
    ai.train()
